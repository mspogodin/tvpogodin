//
//  MapViewController.swift
//  TVpogodin
//
//  Created by WSR on 22/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var editCity: UITextField!
    
    var userDefaults = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let city = self.userDefaults.string(forKey: "cityLabel") {
            self.downloadData(city)
        }
        
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cityFind(_ sender: Any) {
        if let city = self.editCity.text {
            self.userDefaults.set(city, forKey: "cityLabel")
            self.downloadData(city)
        }
        print("5")
    }
    
    @IBAction func cityFindButton(_ sender: Any) {
        if let city = self.editCity.text {
            self.userDefaults.set(city, forKey: "cityLabel")
            self.downloadData(city)
        }
        print("6")
    }
    
    func downloadData(_ city: String) {
        let urlOpWM = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&appid=1e936ee21707e2a418e98dca00877357"
        let url = urlOpWM.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        print(city)
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let lon = json["coord"]["lon"].doubleValue
                let lat = json["coord"]["lat"].doubleValue
                self.showCityOnMap(city: city, lon: lon, lat: lat)
                print("7")
                print(lon)
            case .failure(let error):
                print(error)
            }
        }
        print("8")
    }
    
    func showCityOnMap(city: String, lon: Double, lat: Double) {
        let regionRadius: CLLocationDistance = 10000
        let coordinateRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: lon), latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: false)
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        annotation.title = city
        mapView.addAnnotation(annotation)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
