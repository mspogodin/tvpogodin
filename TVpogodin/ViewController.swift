//
//  ViewController.swift
//  TVpogodin
//
//  Created by WSR on 22/06/2019.
//  Copyright © 2019 Pogodin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var cityTemp: UILabel!
    @IBOutlet weak var iconWeather: UIImageView!
    @IBOutlet weak var mapButton: UIButton!
    
    var userDefaults = UserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateView()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        updateView()
    }
    
    func updateView() {
        print("4")
        self.mapButton.layer.cornerRadius = 50
        if let bg = UIImage(named: self.userDefaults.string(forKey: "bgImg")!) {
            self.backgroundImage.image = bg
            self.backgroundImage.contentMode = .scaleAspectFit
        }
        if let lbl = self.userDefaults.string(forKey: "cityLabel") {
            self.cityLabel.text = lbl
            self.downloadData(lbl)
        }
        else {
            self.cityLabel.text = ""
        }
    }
    
    func downloadData(_ city: String) {
        let urlOpWM = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&appid=1e936ee21707e2a418e98dca00877357"
        var url = urlOpWM.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                self.cityTemp.text = json["main"]["temp"].stringValue
                let urlIcon = "https://openweathermap.org/img/w/" + json["weather"][0]["icon"].stringValue + ".png"
                if let icon = UIImage(data: NSData(contentsOf: NSURL(string: urlIcon)! as URL)! as Data) {
                    self.iconWeather.image = icon
                    self.iconWeather.contentMode = .scaleAspectFit
                }
            case .failure(let error):
                print(error)
            }
        }
        
        let urlFlkr = "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=fe96c1b0698cf5d73c48e7e14623f5f9&tags=" + city + "&format=json&nojsoncallback=?"
        url = urlFlkr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let farm = json["photos"]["photo"][0]["farm"].stringValue
                let server = json["photos"]["photo"][0]["server"].stringValue
                let id = json["photos"]["photo"][0]["id"].stringValue
                let secret = json["photos"]["photo"][0]["secret"].stringValue
                let urlBg = "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg"
                if let bg = UIImage(data: NSData(contentsOf: NSURL(string: urlBg)! as URL)! as Data) {
                    self.backgroundImage.image = bg
                    self.backgroundImage.contentMode = .scaleAspectFill
                }
            case .failure(let error):
                print(error)
            }
        }
    }

}

